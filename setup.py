import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pesuacademy",
    version="0.0.1",
    author="Rithvik Vibhu",
    author_email="rithvikvibhu@gmail.com",
    description="An SDK for PesuAcademy",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/rithvikvibhu/pesu-academy-sdk",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)