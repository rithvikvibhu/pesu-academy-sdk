import requests, json, os, urllib, configparser, re, textwrap

BASE_URL = "https://www.pesuacademy.com/MAcademy/mobile/dispatcher"

class SDK(object):
    '''SDK for PesuAcademy'''

    def __init__(self):
        self.token = ""
        self.userId = ""

    def ppjson(self, json_thing, sort=True, indents=4):
        '''Pretty prints any json-like string/object'''
        if type(json_thing) is str:
            print(json.dumps(json.loads(json_thing), sort_keys=sort, indent=indents))
        else:
            print(json.dumps(json_thing, sort_keys=sort, indent=indents))
        return None

    def sanitizeString(self, string, width=80):
        '''Cleans up urlencoded strings and strips html tags'''
        return textwrap.fill(re.sub(r'<(?:.|\n)*?>', '', re.sub(r'<style((.|\n|\r)*?)<\/style>', '', urllib.parse.unquote(string))).strip(), width)

    def makeRequest(self, action, mode, data={}, auth=True, base_url=BASE_URL):
        '''Helper function that makes requests to PesuAcademy'''
        headers = { "Content-Type": "application/x-www-form-urlencoded" }
        if auth:
            headers["mobileAppAuthenticationToken"] = self.token

        merged_data = { "action": action, "mode": mode, "userId": self.userId }
        merged_data.update(data)

        r = requests.post(base_url, headers=headers, data=merged_data)
        # print(r.text)
        try:
            # ppjson(r.json())
            return r.json()
        except json.decoder.JSONDecodeError as e:
            print("ERROR:\n", action, mode, headers, merged_data, r.text, e)
            return None

    def login(self, creds={}):
        '''Login with username and password'''
        if not creds:
            return None
        s = requests.Session()
        headers = { "User-Agent": "Mozilla/5.0 (Linux; Android 8.1.0; Nexus 5X Build/OPM7.181205.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/73.0.3683.90 Mobile Safari/537.36" }
        data = { "action": 0, "mode": 0, "j_username": creds['username'], "j_password": creds['password'], "j_mobileApp": "YES" }
        r = s.post("http://www.pesuacademy.com/MAcademy/j_spring_security_check", headers=headers, data=data, allow_redirects=True)
        if r.json()['login'] == 'ERROR':
            return None
        if 'mobileAppAuthenticationToken' in r.headers:
            self.token = r.headers['mobileAppAuthenticationToken']
            self.userId = r.json()['userId']
            return True
    # print(login())

    def setUser(self, user):
        '''Set token and userId manually'''
        self.token = user['token']
        self.userId = user['userId']
        return None

    def getClasses(self):
        '''Returns classes'''
        return self.makeRequest(18, 1, {"userId": self.userId}, auth=True)
    # ppjson(getClasses(485))

    def getCourses(self, batchClassId):
        '''Returns courses'''
        return self.makeRequest(18, 2, {"batchClassId": batchClassId}, auth=True)["STUDENT_SUBJECTS"]
    # ppjson(getCourses(485))

    def getCourseInfo(self, subjectId):
        '''Returns course contents'''
        return self.makeRequest(18, 11, {"subjectId": subjectId}, auth=True)["COURSE_CONTENT"]
    # ppjson(getCourseInfo(3656))

    def getCourseContentInfo(self, subjectId, ccId):
        '''Returns course content data'''
        return self.makeRequest(18, 14, {"subjectId": subjectId, "ccId": ccId}, auth=True)
    # ppjson(getCourseContentInfo(5706, 16507))

    def getCourseContentNote(self, courseNoteId):
        '''Returns note'''
        return self.makeRequest(18, 15, {"courseNoteId": courseNoteId}, auth=True)
    # ppjson(getCourseContentNote(74))
