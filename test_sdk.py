import configparser
import pytest #pylint: disable=import-error
from pesuacademy import SDK

# Read config file
Config = configparser.ConfigParser()
Config.read("config.ini")
config = Config["DEFAULT"]

sdk = SDK()

self_data = {}

class Test_SDK(object):

    def test_self(self):
        print("Abc.")

    @pytest.mark.parametrize('inp', ['{"a": 5}', {"a": 5}])
    def test_ppjson(self, inp):
        assert sdk.ppjson(inp) == None

    @pytest.mark.parametrize('inp', ['<div>abc</div>', '<div><style>body {color: red;}</style>abc</div>'])
    def test_sanitizestring(self, inp):
        assert sdk.sanitizeString(inp) == 'abc'

    def test_make_request_error(self):
        invalid_request = sdk.makeRequest(100, 100)
        assert invalid_request == None

    def test_login_with_tokens(self):
        assert sdk.setUser({"token": config["token"], "userId": config["userId"]}) == None

    def test_login_with_creds(self):
        empty = sdk.login()
        assert empty == None
        invalid = sdk.login({"username": 'a', "password": 'a'})
        assert invalid == None
        success = sdk.login({"username": config["username"], "password": config["password"]})
        assert success == True

    def test_get_classes(self):
        classes = sdk.getClasses()
        assert type(classes) is list and len(classes) > 0
        self_data['classId'] = classes[0]['batchClassId']

    def test_get_courses(self):
        courses = sdk.getCourses(batchClassId=self_data['classId'])
        assert type(courses) is list and len(courses) > 0
        self_data['subjectId'] = courses[0]['SubjectId']

    def test_get_course_content(self):
        course_content = sdk.getCourseInfo(subjectId=self_data['subjectId'])
        assert type(course_content) is list
        course_content = list(filter(lambda x: x['CourseContentType'] == 'Course Contents', course_content))
        assert len(course_content) > 0
        self_data['courseContentId'] = course_content[0]['CourseContentId']

    def test_get_course_content_data(self):
        course_content_data = sdk.getCourseContentInfo(subjectId=self_data['subjectId'], ccId=self_data['courseContentId'])
        assert type(course_content_data) is list and len(course_content_data) > 0
        self_data['courseNoteId'] = course_content_data[0]['CourseNoteId']

    def test_get_course_content_note(self):
        course_note_data = sdk.getCourseContentNote(courseNoteId=self_data['courseNoteId'])
        assert type(course_note_data) is dict and 'info' in course_note_data and 'Files' in course_note_data

