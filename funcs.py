from prettytable import PrettyTable
import textwrap
import json, os, urllib, re, configparser
from pesuacademy import SDK

Config = configparser.ConfigParser()
Config.read("config.ini")
config = Config["DEFAULT"]

sdk = SDK()
sdk.setUser({"token": config["token"], "userId": config["userId"]})

def createCoursesListFromNotes():
    coursesList = []
    tmpListOfIds = set()
    for filename in os.listdir('course-contents-data'):
        with open('course-contents-data/'+filename, 'r') as ccdfile:
            note = json.load(ccdfile)[0]['noteContents']
        subjectId = note['info']['subjectId']
        subjectName = note['info']['subjectName']
        subjectCode = note['info']['subjectCode']
        print("CCD file", filename.rstrip('.json'), '\t-', subjectCode, subjectId, subjectName)
        if subjectId not in tmpListOfIds:
            tmpListOfIds.add(subjectId)
            coursesList.append({ "subjectId": subjectId, "subjectCode": subjectCode, "subjectName": subjectName, "searchable": subjectName+" ("+subjectCode+")" })
    sdk.ppjson(coursesList)
    with open('courses-new.json', 'w') as cfile:
        json.dump(coursesList, cfile)

def getCoursesForAllBatchClassId():
    i = 0
    while True:
        courses = sdk.getCourses(batchClassId=i)
        print("Got courses for batchClassId", i, "\tlength:", len(courses))
        if courses:
            pass
        i += 1

def getNotes():
    with open('notes.json', 'w') as f:
        f.write('[\n')
    i = 1
    first_time = True
    while True:
        print('Getting note', i)
        course_note_data = sdk.getCourseContentNote(courseNoteId=i)
        if course_note_data:
            with open('notes.json', 'a') as f:
                if not first_time:
                    f.write(",\n")
                f.write(json.dumps(course_note_data))
        else:
            break
        i += 1
    with open('notes.json', 'a') as f:
        f.write(']\n')

def getCourses():
    courses = sdk.getCourses(batchClassId=310)
    sdk.ppjson(courses)
    # notes = []
    # with open('notes.json', 'w') as f:
    #     f.write('[\n')
    # i = 1
    # first_time = True
    # while True:
    #     print('Getting note', i)
    #     course_note_data = sdk.getCourseContentNote(courseNoteId=i)
    #     if course_note_data:
    #         with open('notes.json', 'a') as f:
    #             if not first_time:
    #                 f.write(",\n")
    #             f.write(json.dumps(course_note_data))
    #     else:
    #         break
    #     i += 1
    # with open('notes.json', 'a') as f:
    #     f.write(']\n')

def getCourseContent():
    filename = 'course-content-test.json'
    with open(filename, 'w') as f:
        f.write('[\n')
    i = 3400
    first_time = True
    while True:
        course_content_data = sdk.getCourseInfo(subjectId=i)
        print('Got content for subjectId', i, '\tlength:', len(course_content_data))
        if course_content_data:
            with open(filename, 'a') as f:
                if not first_time:
                    f.write(",\n")
                else:
                    first_time = False
                f.write(json.dumps(course_content_data))
        # else:
        #     break
        i += 1
    with open(filename, 'a') as f:
        f.write(']\n')

def redownloadCourseContent():
    with open('course-content-3400-to-7900.csv', 'r') as fp:
        for cnt, line in enumerate(fp):
            # print("Line {}: {}".format(cnt, line))
            if cnt:
                subjectId = line.strip().split(',')[0]
                course_content_data = sdk.getCourseInfo(subjectId=subjectId)
                print('Got content for subjectId', subjectId, '\tlength:', len(course_content_data))
                # sdk.ppjson(course_content_data)
                with open('./course-contents/'+str(subjectId+'.json'), 'w') as ccfile:
                    ccfile.write(json.dumps(course_content_data))

def getCourseContentData():
    for filename in os.listdir('course-contents'):
        if filename < "6029.json":
            continue
        with open("course-contents/"+filename, "r") as subjectfile:
            cc = json.load(subjectfile)
        for cc_entry in cc:
            print("Getting course", filename, "- content id", cc_entry["CourseContentId"])
            cc_data = sdk.getCourseContentInfo(subjectId=filename.rstrip(".json"), ccId=cc_entry["CourseContentId"])
            # cc_data = sdk.getCourseContentInfo(subjectId=5706, ccId=16507)
            # sdk.ppjson(cc_data)
            if cc_data:
                for i in range(len(cc_data)):
                    cc_data[i]["noteContents"] = sdk.getCourseContentNote(courseNoteId=cc_data[i]["CourseNoteId"])
                # sdk.ppjson(cc_data)
                with open('./course-contents-data/'+str(cc_entry["CourseContentId"])+'.json', 'w') as ccdfile:
                        ccdfile.write(json.dumps(cc_data))

createCoursesListFromNotes()
print('Done! :)')