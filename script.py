import textwrap, json, os, urllib, re, configparser
from prettytable import PrettyTable
from pesuacademy import SDK

# Read config file
Config = configparser.ConfigParser()
Config.read("config.ini")
config = Config["DEFAULT"]

sdk = SDK()

# Check if already logged in info in config file
if "token" in config and "userId" in config:
    print("Using token and userId from config.")
    sdk.setUser({"token": config["token"], "userId": config["userId"]})
else:
    # Check for credentials in config file
    if "username" in config and "password" in config:
        print("Using username and password creds from config.")
        username = config["username"]
        password = config["password"]
    else:
        # Get credentials by input
        print("Couldn't find any token or credential in config. Asking for input.")
        username = input("Enter username: ")
        password = input("Enter password: ")
    # Login
    if sdk.login({"username": username, "password": password}):
        print("Login successful!")
    else:
        print("Invalid credentials. Try again.")
        exit()


# Get classes
classes = sdk.getClasses()
sdk.ppjson(classes)
t = PrettyTable(['batchClassId', 'className', 'isPromoted', 'studentRollNo'])
for c in classes:
    t.add_row([ c["batchClassId"], c["className"], c["isPromoted"], c["studentRollNo"] ])
print(t)
batch_class_id = input("Enter batchClassId: ")


# Get courses for class
courses = sdk.getCourses(batchClassId=batch_class_id)
t = PrettyTable(['SubjectId', 'SubjectCode', 'SubjectName', 'Credits'])
for c in courses:
    t.add_row([ c["SubjectId"], c["SubjectCode"], c["SubjectName"], c["Credits"] ])
print(t)
subjectId = input("Enter subjectId: ")


# Get course content for course
course_content = sdk.getCourseInfo(subjectId=subjectId)
t = PrettyTable(['CourseContentId', 'CourseContentType', 'Heading', 'CourseContent'])
for c_c in course_content:
    t.add_row([ c_c["CourseContentId"], c_c["CourseContentType"], sdk.sanitizeString(c_c["Heading"]), sdk.sanitizeString(c_c["CourseContent"]) ])
print(t)
cc_id = input("Enter CourseContentId: ")


# Get course content data for course content
course_content_data = sdk.getCourseContentInfo(subjectId=subjectId, ccId=cc_id)
t = PrettyTable(['CourseNoteId', 'Type', 'TopicTitle'])
for cc_data in course_content_data:
    t.add_row([ cc_data["CourseNoteId"], cc_data["Type"], cc_data["TopicTitle"] ])
print(t)
c_note_id = input("Enter courseNoteId: ")


# Get note for course content data
course_note_data = sdk.getCourseContentNote(courseNoteId=c_note_id)

# Display all files
print(f'''
---
Subject: {course_note_data['info']['subjectName']}
Topic Title: {course_note_data['info']['topicTitle']}
Contents: {sdk.sanitizeString(course_note_data['info']['contents'])}
---''')
t = PrettyTable(['noteReferenceId', 'fileName', "link"])
for c_note in course_note_data['Files']:
    t.add_row([ c_note["noteReferenceId"], c_note["fileName"], "https://www.pesuacademy.com/MAcademy/previewUnitTopicFile/"+c_note_id+"/"+urllib.parse.quote_plus(c_note["fileName"]) ])
print(t)


print('Done! :)')