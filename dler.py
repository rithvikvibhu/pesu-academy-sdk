import json, os, urllib, re, configparser
from collections import defaultdict
import questionary # pylint: disable=import-error
from pesuacademy import SDK

Config = configparser.ConfigParser()
Config.read("config.ini")
config = Config["DEFAULT"]

sdk = SDK()
sdk.setUser({"token": config["token"], "userId": config["userId"]})

classes = sdk.getClasses()
# sdk.ppjson(classes)

selected_classes = questionary.checkbox(
    'Which classes do you want to download for?',
    choices=['all']+list(map(lambda x: x['className']+' ('+str(x['batchClassId'])+')', classes))
).ask()
selected_classes = ['Sem-2 (485)']

if 'all' in selected_classes:
    # selected_classes = list(map(lambda x: x['className']+' ('+str(x['batchClassId'])+')', selected_classes))
    selected_classes = list(map(lambda x: '{className} ({batchClassId})'.format(**x), classes))
selected_classes = list(map(lambda x: int(re.search(r'.* \((\d+)\)', x).group(1)), selected_classes))
# sdk.ppjson(selected_classes)

for class_id in selected_classes:
    courses = sdk.getCourses(batchClassId=class_id)
    # sdk.ppjson(courses)
    selected_courses = questionary.checkbox(
        'Which courses do you want to download?',
        choices=['all']+list(map(lambda x: '{SubjectCode} - {SubjectName} ({SubjectId})'.format(**x), courses))
    ).ask()
    selected_courses = ['UE18EE101 - Basic Electrical Engineering (5719)']

    if 'all' in selected_courses:
        selected_courses = list(map(lambda x: x['className']+' ('+str(x['batchClassId'])+')', courses))
    selected_courses = list(map(lambda x: int(re.search(r'.* \((\d+)\)', x).group(1)), selected_courses))
    # sdk.ppjson(selected_courses)

    for course_id in selected_courses:
        course_contents = sdk.getCourseInfo(subjectId=course_id)
        # sdk.ppjson(course_contents)
        groups = defaultdict(list)
        choices = ['All']
        for cc in course_contents:
            groups[cc['CourseContentType']].append(cc)
        # sdk.ppjson(groups)
        for group in groups:
            choices.append(questionary.Separator('=== '+group+' ==='))
            choices.extend(list(map(lambda x: f'{sdk.sanitizeString(x["Heading"])} ({x["CourseContentId"]})', groups[group])))
        selected_course_contents = questionary.checkbox(
            'Pick course content to download.',
            choices=choices
        ).ask()
        # sdk.ppjson(selected_course_contents)
        selected_course_contents = ['Three Phase Balanced Systems (7133)']

        if 'all' in selected_course_contents:
            selected_course_contents = list(map(lambda x: x['className']+' ('+str(x['batchClassId'])+')', course_contents))
        selected_course_contents = list(map(lambda x: int(re.search(r'.* \((\d+)\)', x).group(1)), selected_course_contents))
        # sdk.ppjson(selected_course_contents)

        for cc_id in selected_course_contents:
            cc_data = sdk.getCourseContentInfo(subjectId=course_id, ccId=cc_id)
            # sdk.ppjson(cc_data)
            selected_cc_data = questionary.checkbox(
                'Select notes to download.',
                choices=['all']+list(map(lambda x: f'{sdk.sanitizeString(x["TopicTitle"])} ({x["CourseNoteId"]})', cc_data))
            ).ask()
            selected_cc_data = ['Three Phase Balanced Systems (108)']

            if 'all' in selected_cc_data:
                selected_cc_data = list(map(lambda x: x['className']+' ('+str(x['batchClassId'])+')', courses))
            selected_cc_data = list(map(lambda x: int(re.search(r'.* \((\d+)\)', x).group(1)), selected_cc_data))
            sdk.ppjson(selected_cc_data)