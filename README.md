# PesuAcademy SDK

[![pipeline status](https://gitlab.com/rithvikvibhu/pesu-academy-sdk/badges/master/pipeline.svg)](https://gitlab.com/rithvikvibhu/pesu-academy-sdk/commits/master) [![Coveralls Coverage Status](https://coveralls.io/repos/gitlab/rithvikvibhu/pesu-academy-sdk/badge.svg?branch=HEAD)](https://coveralls.io/gitlab/rithvikvibhu/pesu-academy-sdk?branch=HEAD) [![GitLab coverage report](https://gitlab.com/rithvikvibhu/pesu-academy-sdk/badges/master/coverage.svg)](https://gitlab.com/rithvikvibhu/pesu-academy-sdk/commits/master)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/rithvikvibhu/pesu-academy-sdk/blob/master/LICENSE)

This is an unofficial sdk for PESU Academy.